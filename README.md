# Mars Defence Force

**What did I do:**
- Group leader (7 people)
- Game idea
- Programmed players
- Integrated player animations
- Programmed weapons
- Sound integrated
- Light
- Effects
- Balancing

It took us 7 months from the game idea to the final realization. 
Our team consisted of 2 artists, 3 programmers and an audio engineer. 
In this project, I learned a lot about group management and leadership. I also deepened my knowledge of agile project management. We were supported by one of our external lecturers, who also coaches companies in the field of project management on a freelance basis. 
An additional tool for displaying in-game statistics of the individual rounds was planned, but was unfortunately not completed due to illness of a group member. For this reason, the round statistics are saved as XML after a round.

I also learned a lot of new know-how in this project. This includes level design, light and particles. I have also been able to deepen my knowledge of the Unreal Engine in the area of visual scripting. 

**The game**

The game is a combination of a tower defense game and a 3P shooter. This combination gives the player the opportunity to intervene in the action. At the beginning the player is very strong with his weapons and has to shoot at the opponents to get his first money (Argent). As the game progresses, the enemies get stronger and stronger, so the player has to improve the towers and upgrade his 3 weapons. At the spawn he can turn his handgun into a sniper. He can turn his minigun into a stationary LMG, which can also be collected. And his harpoon into a shotgun. Each weapon plays differently and feels correspondingly different.

We also created a little "Behinde the Scenes" in german:

[![Everything Is AWESOME](https://img.youtube.com/vi/TjnC4EnVqmo/0.jpg)](https://www.youtube.com/watch?v=TjnC4EnVqmo "Mars Defence Force")

---------------------------------------------------------

**Was habe ich gemacht:**
- Gruppenleitung (7 Personen)
- Spieleidee
- Spieler programmiert
- Spieler Animationen eingebunden
- Waffen programmiert
- Sound eingebunden
- Licht
- Effekte
- Balancing

Von der Spiele Idee bis zur fertigen Umsetzung haben wir 7 Monate gebraucht. 
Unser Team bestand aus 2 Artist, 3 Programmierern und einem Audio Engenier. 
In diesem Projekt habe ich besonders viel was Gruppenleitung und Gruppenführung angeht gelernt. Mit dazu auch das agile Projektmanagment nochmals vertieft. Unterstützung hatten wir dabei von einem unserer externen Dozenteninnen, welche auch Freiberuflich in der Industrie im Bereich Projektmanagment die Unernehmen coacht. 
Eine zusätzliches Tool zum Anzeigen von InGame Statistiken der einzelnen RUnden war geplant, wurde aber leider aus Krankheitsgründen eines Gruppenmitglieds nicht fertig gestellt. Aus diesem Grund wird nach einer Runde die Runden Statistiken als XML abgespeichert.

Auch habe ich in diesem Projekt viel neues Know How gelernt. Dazu gehört Level Desinge, Licht und Partikels. Auch habe ich eine ganz gute Vertiefung in die Unreal Engine machen können im Bereich des Visual Scripting. 

**Das Spiel**

Das Spiel ist eine Spiele Kombination aus einem Tower Defence und einem 3P-Shooter. Durch diese Kombination hat der Spieler die Möglichkeit selber in des Geschehen einzu greifen. Zu beginn ist der Spieler sehr stark mit seinen Waffen und muss auf die Gegner schießen um auch sein erstes Geld (Argent) zu erhalten. Im Laufe des Spieles werden die Gegner immer Stärker, sodass der Spieler die Türme verbessern muss, aber auch seine 3 Waffen upgraden muss. Am Spawn kann er aus seiner Handfeuerwaffe eine Sniper machen. Aus seiner Minigun ein stationäres LMG, welches auch eingesammelt werden kann. Und aus seiner Harpune eine Shotgun. Jede Waffe spielt sich anders und fühlt sich dem entsprechend anders an.

Es wurde auch ein kleines "Behinde the Scenes" von uns erstellt:

[![Everything Is AWESOME](https://img.youtube.com/vi/TjnC4EnVqmo/0.jpg)](https://www.youtube.com/watch?v=TjnC4EnVqmo "Mars Defence Force")
